import {post} from '../service/api'
import {LOGIN_FAIL, LOGIN_SUCCESS, LOGOUT} from './action-types'
import moment from 'moment'
import {addSnackbarEntry} from './snackbars-action'
import {ADMIN, TUTOR, USER} from '../service/roles'

export function successLogin(role, username, login) {
    return dispatch => {
        dispatch({
            type: LOGIN_SUCCESS,
            payload: {
                role, username, login
            }
        })
    }
}

export function failLogin(error) {
    return dispatch => {
        dispatch({
            type: LOGIN_FAIL,
            payload: error
        })
        if (error) {
            dispatch(addSnackbarEntry('error', error))
        }
    }
}

export function login(login, password) {
    const user = login
    return async dispatch => {
        try {
            const response = await post('/auth/login', {
                login: user,
                password
            })

            const {access, key, login} = response.data
            if (access && key) {
                const tokenData = parseJwt(access)

                localStorage.setItem('token', access)
                localStorage.setItem('key', key)
                localStorage.setItem('id', tokenData.id)
                localStorage.setItem('login', login)

                const role = extractRole(tokenData)
                const username = tokenData.sub
                dispatch(successLogin(role, username, login))
            } else {
                dispatch(failLogin('Пароль или логин неверные'))
            }
        } catch (e) {
            console.log(e)
            dispatch(failLogin('Пароль или логин неверные'))
        }
    }
}

function extractRole(tokenData) {
    const roles = tokenData.roles
    let role = USER

    if (roles.indexOf(TUTOR) !== -1) {
        role = TUTOR
    }
    if (roles.indexOf(ADMIN) !== -1) {
        role = ADMIN
    }

    return role
}

export function checkAuth() {
    return async dispatch => {
        const access = localStorage.getItem('token')
        const key = localStorage.getItem('key')

        if (!access) {
            dispatch(failLogin())
        } else {
            const tokenData = parseJwt(access)

            const now = moment()
            const expDate = moment(tokenData.exp * 1000)

            const role = extractRole(tokenData)
            const username = tokenData.sub

            post('/auth/key', {key,access})
                .then(response => {
                    if (!response.data) {
                        dispatch(failLogin())
                        localStorage.removeItem('token')
                    }
                })

            if (expDate.isAfter(now)) {
                dispatch(successLogin(role, username, username))
            } else {
                dispatch(failLogin())
                localStorage.removeItem('token')
            }
        }
    }
}

function parseJwt(token) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url
        .replace(/-/g, '+')
        .replace(/_/g, '/')

    const jsonPayload = decodeURIComponent(
        atob(base64)
            .split('')
            .map(c => {
                return '%'
                    + ('00' + c.charCodeAt(0).toString(16))
                        .slice(-2);
            }).join(''));

    return JSON.parse(jsonPayload);
}

export function logout() {
    return dispatch => {
        localStorage.removeItem('token')
        localStorage.removeItem('key')
        localStorage.removeItem('login')
        localStorage.removeItem('id')

        dispatch({
            type: LOGOUT
        })
    }
}