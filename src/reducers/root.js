import {combineReducers} from 'redux'
import snackbars from "./snackbars";
import login from "./login";

const rootReducer = combineReducers({
    snackbars, login
})

export default rootReducer