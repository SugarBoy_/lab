import React from 'react'
import BugReportIcon from '@material-ui/icons/BugReport';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';
import KeyboardIcon from '@material-ui/icons/Keyboard';
import HomeIcon from '@material-ui/icons/Home';
import DoneIcon from '@material-ui/icons/Done';
import PanToolIcon from '@material-ui/icons/PanTool';
import ClearIcon from '@material-ui/icons/Clear';
import AttachmentIcon from '@material-ui/icons/Attachment';
import SettingsIcon from '@material-ui/icons/Settings';
import {Tooltip} from "@material-ui/core";

export function taskTypes() {
    return ['PROGRAMMING', 'DEBUG', 'ASSIMILATION', 'ADVANCED']
}

export function taskTypeToDisplayName(type) {
    switch (type) {
        case 'PROGRAMMING':
            return 'Разработка'
        case 'DEBUG':
            return 'Отладка'
        case 'ASSIMILATION':
            return 'Понимание'
        case 'HOME':
            return 'Домашнее задание'
        case 'DONE':
            return 'Выполнено'
        case 'CHECK':
            return 'Ожидается проверки'
        case 'NOCONFIRM':
            return 'Провалено'
        case 'ADVANCED':
            return 'Продвинутое'
        case 'CLEAR':
            return 'Новое задание'
        default:
            return type
    }
}

export function taskTypeToIcon(type) {
    switch (type) {
        case 'DEBUG':
            return <BugReportIcon style={{
                color: 'orangered'
            }}/>
        case 'ASSIMILATION':
            return <PriorityHighIcon style={{
                color: 'tomato'
            }}/>
        case 'HOME':
            return <HomeIcon style={{
                color: 'forestgreen'
            }}/>
        case 'DONE':
            return <DoneIcon style={{
                color: 'limegreen',
                margin: '2px 8px'
            }}/>
        case 'CHECK':
            return <PanToolIcon style={{
                color: 'orange',
                margin: '2px 8px'
            }}/>
        case 'NOCONFIRM':
            return <ClearIcon style={{
                color: 'tomato',
                margin: '2px 8px'
            }}/>
        case 'CLEAR':
            return <AttachmentIcon style={{
                color: 'deepskyblue',
                margin: '2px 8px'
            }}/>
        case 'ADVANCED':
            return <SettingsIcon style={{
                color: 'dimGray',
                margin: '2px 8px'
            }}/>
        default:
            return <KeyboardIcon style={{
                color: 'darkviolet'
            }}/>
    }
}

export function taskTypeToIconWithDescription(type) {
    return (
        <Tooltip title={taskTypeToDisplayName(type)}>
                {taskTypeToIcon(type)}
        </Tooltip>
    )

}