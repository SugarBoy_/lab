import React from 'react'
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import './style.css'
import {addSnackbarEntry} from "../../actions/snackbars-action";
import {connect} from "react-redux";
import {isEmpty} from "../../service/utils";
import {login} from "../../actions/login-action";

class LoginForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            playername: '',
            password: '',
            error: []
        }
    }

    onChange(e, field) {
        const {value} = e.target
        switch (field) {
            case 'playername':
                this.setState({playername: value})
                break

            case 'password':
                this.setState({password: value})
                break
        }
    }

    submit() {

        const {playername, password} = this.state
        const error = []

        if (isEmpty(playername)) {
            error.push('playername')
        }

        if (isEmpty(password)) {
            error.push('password')
        }

        if (error.length !== 0) {
            this.setState({error})
        } else {
            this.props.login(playername, password)
        }
    }

    render() {
        const state = this.state
        const error = state.error

        return (
            <>

                <div className="form">
                    <h1>Авторизация</h1>

                    <TextField
                        label="Имя в игре или почта"
                        value={state.playername}
                        error={error.includes('playername')}
                        onChange={event => this.onChange(event, 'playername')}
                    />
                    <br/>
                    <TextField
                        label="Пароль"
                        type="password"
                        value={state.password}
                        error={error.includes('password')}
                        onChange={event => this.onChange(event, 'password')}
                    />
                    <Button
                        onClick={() => this.submit()}
                        style={{
                            marginTop: '8px'
                        }}
                    >
                        Учиться
                    </Button>
                </div>
            </>
        )
    }
}

const mapDispatchToProps = {
    addSnackbarEntry,
    login
}

export default connect(null, mapDispatchToProps)(LoginForm)