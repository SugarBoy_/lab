import React from 'react'
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import {get, post} from "../../service/api"
import {connect} from "react-redux";
import './style.css'
import {TextField} from "@material-ui/core";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import {addSnackbarEntry} from "../../actions/snackbars-action";
import {isEmpty} from "../../service/utils";

class PopupUserList extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            users: [],
            playername: '',
            role: 'VIEWER'
        }

        this.loadUsers()
    }

    async loadUsers() {
        const {url} = this.props.params
        await get(`/ide/project/${url}/users`)
            .then(response => {
                this.setState({
                    users: response.data
                })
            })
    }

    changePlayername(e) {
        this.setState({
            playername: e.target.value
        })
    }

    changeRole(e) {
        this.setState({
            role: e.target.value
        })
    }

    async invite() {
        const {url} = this.props.params
        const {playername, role} = this.state
        if (isEmpty(playername)) {
            this.props.addSnackbarEntry('warning', 'Заполните все поля')
            return
        }

        await post(`/ide/project/invite/`, {
            url,
            playername,
            role
        }).then(() => {
            this.loadUsers()
        }).catch(error => {
            error.response &&
            error.response.data.message &&
            this.props.addSnackbarEntry('warning', error.response.data.message)
        })
    }

    render() {
        const {users, playername, role} = this.state
        const {username} = this.props.login

        return (
            <Dialog open={true} onClose={() => this.props.onClose()}>
                <DialogContent>
                    <h2>Участника проекта: </h2>

                    <div className="users">
                        {
                            users.filter(user => user.username !== username).map(user =>
                                <div className="user">
                                    <span className="user span-name">{user.playername}</span>
                                    <span className="user span-role">{user.role}</span>
                                </div>
                            )
                        }

                        <h3>Пригласить:</h3>

                        <div className="user">
                            <TextField
                                className="user span-name"
                                placeholder="Имя в игре"
                                value={playername}
                                onChange={(e) => this.changePlayername(e)}
                            />
                            <Select
                                className="user span-role"
                                value={role}
                                onChange={(e) => this.changeRole(e)}
                                style={{width: '180px'}}
                            >
                                <MenuItem value={"VIEWER"}>Просмотр</MenuItem>
                                <MenuItem value={"TUTOR"}>Преподаватель</MenuItem>
                            </Select>
                        </div>
                    </div>

                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.invite()} color="primary">
                        Пригласить
                    </Button>
                    <Button onClick={() => this.props.onClose()} color="primary">
                        Отмена
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

const mapStateToProps = (state) => ({
    login: state.login
})

const mapDispatchToProps = ({
    addSnackbarEntry
})

export default connect(mapStateToProps, mapDispatchToProps)(PopupUserList)