import React from "react";
import './style.css'
import {get} from "../../service/api";
import StarRoundedIcon from "@material-ui/icons/StarRounded"
import LockRoundedIcon from "@material-ui/icons/LockRounded"
import SimpleHeader from "../headers/SimpleHeader";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from "@material-ui/core/SvgIcon/SvgIcon";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import {isEmpty} from "../../service/utils";
import Button from "@material-ui/core/Button";
import Parser from 'html-react-parser';
import {taskTypeToIconWithDescription} from "../tasktype";
import {Link} from "react-router-dom";

class ModulesList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            course: undefined,
            redirectTo: undefined,
            selected: undefined,
            modules: [],
            profiles: [],
            lessons: [],
        }
    }

    async componentDidMount() {
        const {modules, profiles} = this.state
        const userId = localStorage.getItem('id')

        const loadModule = async (blockId) => {
            await get(`/course/block/${blockId}`)
                .then(response => modules.push(response.data))
                .catch(error => console.log(error.data))
        }

        const loadModuleProfile = async (blockId) => {
            await get(`/profile/block/${userId}/${blockId}`)
                .then(response => profiles.push(response.data))
                .catch(error => console.log(error.data))
        }

        const loadCourse = async (courseId) => {
            await get(`/course/${courseId}`)
                .then(async response => await response.data.blocks.map(async blockId => {
                    const course = response.data
                    await loadModule(blockId);
                    await loadModuleProfile(blockId)
                    this.setState({course})
                }))
                .catch(error => console.log(error.data))
        }

        const {courseId} = this.props.match.params
        await loadCourse(courseId)
    }

    selectModule(blockId) {
        this.setState({selected: blockId})

        const {modules} = this.state

        const findSelectedModules = () => {
            return modules.find(module => module.id === blockId)
        }

        const loadLessons = async () => {
            findSelectedModules().lessons.forEach(async (lessonId, k) => {
                await get(`/lesson/${lessonId}`)
                    .then(async response => (
                        this.state.lessons[k] = response.data
                    ))
                    .catch(error => {
                        console.log(error.data)
                    })
                this.setState({lessons: this.state.lessons})
            })
        }

        loadLessons()
    }

    renderModules() {
        const imgs = [
            "https://static.slimecode.ru/img/ide/module1.jpg",
            "https://static.slimecode.ru/img/ide/module2.jpg",
            "https://static.slimecode.ru/img/ide/module3.jpg",
            "https://static.slimecode.ru/img/ide/module.jpg"
        ]

        const {course, modules, profiles} = this.state

        const findModuleById = (blockId) => {
            return modules.find(module => module.id === blockId)
        }

        const findProfileById = (blockId) => {
            return profiles.find(profile => profile.blockId === blockId)
        }

        return (

            <div className="modules">
                {
                    course && modules.length === course.blocks.length &&
                    profiles.length === course.blocks.length &&
                    course.blocks.map((blockId, k) => {

                        const module = findModuleById(blockId)
                        const profile = findProfileById(blockId)
                        const available = profile.available

                        return (
                            <div key={blockId} onClick={() => available && this.selectModule(blockId)}
                                 className="module"
                                 style={{
                                     background: 'url("' + imgs[k < 3 ? k : 3] + '") no-repeat center',
                                     backgroundSize: 'cover'
                                 }}>
                                {available ?
                                    <div>
                                        <div className="title">
                                            <h2>{module.title}</h2>
                                        </div>

                                        <div className="footer">
                                            <div className="stars-bar">
                                                <StarRoundedIcon style={{
                                                    width: '50px',
                                                    height: '50px',
                                                    color: 'orange',
                                                    margin: '0 auto',
                                                    display: 'block'
                                                }}/>
                                                <div style={{display: 'flex', width: '100%'}}>
                                                    <div style={{margin: '0 auto', display: 'flex'}}>
                                                        <h2>{profile.stars}</h2><h3>&nbsp;/&nbsp;{profile.maxStars}</h3>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="progress">
                                                <h2>{(profile.progress * 100).toFixed(0)}%</h2>
                                            </div>
                                        </div>
                                    </div>
                                    :
                                    <>
                                        <div style={{
                                            position: 'absolute',
                                            width: '100%',
                                            height: '100%',
                                            display: 'block',
                                            background: 'rgba(25,25,25,0.6)',
                                            borderRadius: '2vh'
                                        }}>
                                            <div className="title">
                                                <h2>{module.title}</h2>
                                            </div>

                                            <div className="footer">
                                                <div className="lock">
                                                    <LockRoundedIcon style={{
                                                        maxWidth: '200px',
                                                        width: '75%',
                                                        height: 'auto',
                                                        margin: '0 auto',
                                                        display: 'block',
                                                        color: 'tomato'
                                                    }}/>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                }
                            </div>
                        )
                    })

                }
            </div>
        )
    }

    renderLessons() {
        const {lessons, profiles, modules, selected} = this.state

        const findProfileBySelected = () => {
            return profiles.find(profile => profile.blockId === selected)
        }

        const findLessonProfileById = (profile, lessonId) => {
            return profile.lessons.find(lesson => lesson.lessonId === lessonId)
        }

        const profile = findProfileBySelected()

        return (
            <div className="lessons">
                {
                    lessons && lessons.map(lesson => {

                        const lessonProfile = findLessonProfileById(profile, lesson.id)
                        return (

                            <ExpansionPanel expanded={lessonProfile.available ? undefined : false} key={lesson.id}>
                                <ExpansionPanelSummary
                                    expandIcon={<ExpandMoreIcon/>}
                                >
                                    {taskTypeToIconWithDescription(lessonProfile.available ? 'DONE' : 'NOCONFIRM')}
                                    <h3 className="task">{lesson.title}</h3>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <div className="details">
                                        {
                                            !isEmpty(lesson.description)
                                            &&
                                            <div className="description">
                                                {Parser(lesson.description)}
                                            </div>
                                        }

                                        <div className="buttons">
                                            <Button className="button" color="primary" variant="contained"
                                                    component={Link} to={`/lesson/${lesson.id}`}
                                            >
                                                Перейти к уроку
                                            </Button>
                                        </div>
                                    </div>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        )
                    })
                }
            </div>
        )
    }

    onBackClick() {
        this.setState({selected: undefined, lessons: []})
    }


    render() {
        const {selected} = this.state

        const isRedirect = () => !this.state.selected

        return (
            <>
                {<SimpleHeader isRedirect={() => isRedirect()} onBackClick={() => this.onBackClick()}/>}
                {selected ? this.renderLessons() : this.renderModules()}
            </>
        )
    }
}

export default ModulesList