import React from 'react'
import './style.css'


class Pide extends React.Component {

    componentDidMount() {
        const {input} = this.state.lines[0]
        input.focus()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        console.log('update', this.state.selected)
        const state = this.state;
        const {selected} = state
        if (prevState.selected !== selected) {
            const oldLine = prevState.lines[prevState.selected]
            const oldInput = oldLine.input

            console.log(oldInput.selectionStart)

            this.state.lines.map((line, k) => {
                    if (k === this.state.selected) {
                        const input = this.state.lines[k].input
                        const length = input.value.length
                        input.focus()
                        input.value = ''
                        input.value = line.text
                        setTimeout(() => {
                            input.selectionStart = oldInput.selectionStart
                            input.selectionEnd = oldInput.selectionStart
                        }, 0)

                    }
                }
            )
        }
    }

    onFocus(e, k) {
        console.log('onFocus')
        this.setState({
            selected: k
        })
    }

    constructor(props) {
        super(props)
        this.state = {
            lines: [
                {
                    text: 'for x',
                    input: undefined,
                },
                {
                    text: '2dasdsad',
                    input: undefined,
                },
                {
                    text: 'world.get(x, y, z)',
                    input: undefined,
                },
                {
                    text: '4dsad',
                    input: undefined,
                },
                {
                    text: '5',
                    input: undefined,
                },
                {
                    text: '6',
                    input: undefined,
                }
            ],
            selected: 0,
            index: undefined
        }
    }

    print(line, k) {
        return (
            <div className="line" style={{flexDirection: 'row'}}>
                <div> {k + 1} </div>
                <input
                    key={k}
                    onFocus={e => this.onFocus(e, k)}
                    ref={ref => this.setRef(k, ref)}
                    value={line.text}
                    onKeyDown={e => this.keyPress(e)}
                />
            </div>
        )
    }

    setRef(k, ref) {
        this.state.lines[k] = {
            ...this.state.lines[k],
            input: ref
        }
    }

    keyPress(e) {
        console.log(e.key, ' is pressed')
        const {selected} = this.state
        if (e.key === 'Enter') {
            this.addLineAfter(selected)
        } else if (e.key === "ArrowUp") {
            this.changeLineTo(selected - 1)
        } else if (e.key === "ArrowDown") {
            this.changeLineTo(selected + 1)
        }
    }

    addLineAfter(afterLine) {

    }

    changeLineTo(lineNumber) {
        const {lines} = this.state
        if (lineNumber >= 0 && lineNumber <= lines.length)
            this.setState({
                selected: lineNumber
            })
    }

    render() {
        this.state.inputs = [];
        return (
            <div>
                <div className="ide">
                    <div style={{top: '65pt', left: '120px', color: 'orangered', position: 'relative', zIndex: 10}}>
                        |
                    </div>
                    <div style={{position: 'relative'}}>
                        {
                            this.state.lines.map((line, k) => this.print(line, k))
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default Pide;

