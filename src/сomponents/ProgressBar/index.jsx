import React from "react";
import './style.css'

class ProgressBar extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {

        const height = (this.props.height ? this.props.height : 4) + 'px'
        const percent = this.props.percent + '%'
        const unpercent = (100 - this.props.percent) + '%'

        return (
        <div>
            <div className="bar">
                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <div style={{width: percent, backgroundColor: '#ff3e2b', height: height}}>
                        &nbsp;
                    </div>
                    <div style={{width: unpercent, backgroundColor: '#ffbc82', height: height}}>
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export default ProgressBar