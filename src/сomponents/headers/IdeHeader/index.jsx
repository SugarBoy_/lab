import React from 'react'
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import PersonIcon from "@material-ui/icons/Person";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {connect} from "react-redux";
import {logout} from "../../../actions/login-action";
import {Link} from "react-router-dom";
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import SaveIcon from '@material-ui/icons/Save';
import LoopIcon from '@material-ui/icons/Loop';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import PopupUserList from "../../PopupUserList";
import StopIcon from '@material-ui/icons/Stop';

class Header extends React.Component {

    constructor(props) {
        super(props)

        this.props = {
            match: props.match
        }

        this.state = {
            logoLoaded: true,
            openUserList: false
        }

        this.style = {
            AppBar: {
                backgroundColor: '#101727'
            },
            Toolbar: {
                display: 'flex',
                flexDirection: 'row'
            },
            MenuIcon: {
                position: 'absolute',
            },
            Icon: {
                color: '#cccfd3'
            },
            LoginButton: {
                position: 'absolute',
                right: 0,
                color: '#cccfd3'
            },
            Logo: {
                height: '40px',
                cursor: 'pointer',
                margin: '0 auto'
            }
        }
    }

    onLogout() {
        this.props.logout()
    }

    openUserList(status) {
        this.setState({
            openUserList: status
        })
    }

    render() {
        const {style} = this;

        const {openUserList} = this.state
        const {url, param} = this.props.match
        const isIde = url.startsWith('/project/')

        return (
            <>
                {openUserList && <PopupUserList onClose={() => this.openUserList(false)} params={this.props.match.params}/>}
                <AppBar position="fixed" style={style.AppBar}>
                    <Toolbar style={style.Toolbar}>

                        {isIde && (
                            <div style={style.MenuIcon}>
                                <IconButton edge="start" aria-label="menu"
                                            style={{color: '#6ad366'}}
                                            onClick={() => this.props.onRun()}
                                >
                                    <PlayArrowIcon/>
                                </IconButton>
                                <IconButton edge="start" aria-label="menu"
                                            style={{color: 'tomato', padding: '8px'}}
                                            onClick={() => this.props.onStop()}
                                >
                                    <StopIcon fontSize="large"/>
                                </IconButton>
                                <IconButton edge="start" aria-label="menu"
                                            style={{color: '#306998'}}
                                            onClick={() => this.props.onSave()}
                                >
                                    <SaveIcon/>
                                </IconButton>
                                <IconButton edge="start" aria-label="menu"
                                            style={{color: '#ffd562'}}
                                            onClick={() => this.props.onUpload()}
                                >
                                    <LoopIcon/>
                                </IconButton>

                                <IconButton edge="start" aria-label="menu"
                                            style={{color: '#cccfd3'}}
                                            onClick={() => this.openUserList(true)}
                                >
                                    <PersonAddIcon/>
                                </IconButton>
                            </div>
                        )}

                        {this.state.logoLoaded &&
                        <img
                            src="https://static.slimecode.ru/img/ide/icon.png"
                            style={style.Logo}
                            onClick={() => window.scrollTo(0, 0)}
                        />
                        }

                        {
                            this.props.isLoggedIn ?
                                <IconButton edge="start" aria-label="menu" style={style.LoginButton}
                                            onClick={() => this.onLogout()}
                                            component={Link}
                                            to="/"
                                >
                                    <ExitToAppIcon/>
                                </IconButton>
                                :
                                <IconButton edge="start" aria-label="menu" style={style.LoginButton}>
                                    <PersonIcon/>
                                </IconButton>
                        }
                    </Toolbar>
                </AppBar>
            </>
        )
    }
}

const mapDispatchToProps = {
    logout
}

const mapStateToProps = (state) => ({
    isLoggedIn: state.login.isLoggedIn
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)