import React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar/AppBar";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import './style.css'
import Button from "@material-ui/core/Button";
import {connect} from "react-redux";
import {logout} from "../../../actions/login-action";
import {Link} from "react-router-dom";

class SimpleHeader extends React.Component {

    constructor(props) {
        super(props)

        this.text = props.text ? props.text : "Назад"
        this.isRedirect = props.isRedirect ? props.isRedirect : () => true
        this.to = props.to ? props.to : '/'
        this.onBackClick = props.onBackClick ? props.onBackClick : () => console.log('back')
        this.hasBack = props.hasBack !== undefined ? props.hasBack : true

        this.style = {
            AppBar: {
                backgroundColor: '#101727'
            },
            Toolbar: {
                display: 'flex',
                flexDirection: 'row',
                padding: 0
            },
            icon: {
                color: '#cccfd3',
                margin: '20px 0',
            },
            LoginButton: {
                position: 'absolute',
                right: 0,
                color: '#cccfd3'
            },
            Logo: {
                height: '40px',
                cursor: 'pointer',
                margin: '0 auto'
            }
        }
    }


    render() {
        const style = this.style
        const {text, isRedirect, to, onBackClick, hasBack} = this

        return (
            <div>
                <AppBar position="fixed" style={style.AppBar} className="appbar">
                    <Toolbar style={style.Toolbar}>
                        {hasBack && (isRedirect() ?
                            <Button className="button" component={Link} to={to}>
                                <ArrowBackIosIcon style={style.icon}/>
                                <h4>{text}</h4>
                            </Button>
                            :
                            <Button className="button" onClick={() => onBackClick()}>
                                <ArrowBackIosIcon style={style.icon}/>
                                <h4>{text}</h4>
                            </Button>
                        )}


                        <div className="barlogo"
                             onClick={() => !this.hasBack && window.open('https://slimecode.ru', '_blank')}>
                            <img src="https://static.slimecode.ru/img/ide/icon.png"/>
                        </div>

                        <Button className="button" style={{right: 0}} onClick={() => this.props.logout()}>
                            <h4>Выйти</h4>
                        </Button>

                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

const mapDispatchToProps = {
    logout
}

const mapStateToProps = (state) => ({
    isLoggedIn: state.login.isLoggedIn
})

export default connect(mapStateToProps, mapDispatchToProps)(SimpleHeader)