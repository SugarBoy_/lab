import React from "react";
import PlayerProfile from "../PlayerProfile";

class UserProfile extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        const {userId} = this.props.match.params

        return (
            <div className="main">
                <PlayerProfile userId={userId}/>
            </div>
        )
    }

}

export default UserProfile