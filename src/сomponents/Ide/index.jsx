import React from 'react'
import './style.css'
import {get, patch, post} from "../../service/api"
import Highlight from 'react-highlight.js'
import {connect} from "react-redux";
import hljs from 'highlight.js';
import {addSnackbarEntry} from "../../actions/snackbars-action";
import {isEmpty} from "../../service/utils";
import Header from "../headers/IdeHeader";

class Ide extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            code: "Loading..",
            textarea: false,
            selectedLines: [0],
            pt: 0
        }

        this.pressedKeys = []

        this.loadFile()
    }

    componentDidMount() {
        this.tick()
        hljs.initHighlightingOnLoad()
    }

    async loadFile() {
        const {url, file} = this.props.match.params
        await get(`/ide/file/${url}/${file}`)
            .then(response => {
                const {code} = response.data
                this.setState({code})
            })
    }

    async runProject() {
        const {url} = this.props.match.params
        const {username} = this.props.login
        const {code} = this.state
        await post('/ide/project/run', {url, username, code})
        this.props.addSnackbarEntry('success', 'Код был отправлен!')
    }

    tick() {
        const instance = this

        setTimeout(async function () {
            await instance.callbackActions()
            instance.tick()
        }, 5);
    }

    async callbackActions() {
        const {pt} = this.state
        const {url, file} = this.props.match.params

        const checkActions = async () => {
            await get(`/ide/action/${url}/${file}/${pt}`)
                .then(response => {
                    const {actions, pt} = response.data

                    this.setState({pt})

                    actions.map(action => this.restoreAction(action))

                    return response.data
                })
        }

        return checkActions()
    }

    onChange(e) {
        const code = e.target.value
        const {username} = this.props.login

        console.log('code', code)
        this.changeSelectedLines(e)
        this.setState({code})

        const action = {
            type: 'insert',
            payload: {username, code}
        }

        this.sendAction(action)
    }

    restoreAction(action) {
        const {textarea} = this.state
        const {username} = this.props.login

        if (action.pt >= this.state.pt && action.payload.username !== username) {

            if (this.state.textarea) {
                const {selectionStart, selectionEnd} = this.state.textarea

                textarea && setTimeout(async function () {
                    textarea.selectionStart = selectionStart
                    textarea.selectionEnd = selectionEnd
                }, 0);
            }

            const code = action.payload.code
            this.setState({code})
        }
    }

    sendAction(action) {
        const {pt} = this.state
        const {url, file} = this.props.match.params
        post(`/ide/action/${url}/${file}/${pt}`, action)
            .then(response =>
                this.setState({pt: response.data.pt})
            )
        if (pt % 5 === 0) this.saveCode()
    }

    saveCode() {
        const {url, file} = this.props.match.params
        const {code} = this.state

        patch(`/ide/file/${url}/${file}`, {code})
    }

    async uploadCode() {
        const {url, file} = this.props.match.params
        await get(`/ide/file/${url}/${file}`)
            .then(response => {
                const {code} = response.data
                this.setState({code})
            })
    }

    stopProject() {
        const {url} = this.props.match.params
        const {username} = this.props.login
        const code = 'from api.api import Api\nApi().error(\'Stoped\')'

        post('/ide/project/run', {
            url, username, code
        })

        this.props.addSnackbarEntry('error', 'Код был остановлен!')
    }

    handleKeyUp(e) {
        /*const {textarea, selectedLines} = this.state
        if (!textarea)
            return false

        const {selectionStart, selectionEnd, value} = textarea
        const isSelectedNothing = selectionStart === selectionEnd
        const keys = this.pressedKeys

        let code = this.state.code
        let selectionIndex = selectionEnd

        const changeAt = (str, start, end, updateFunc) => str.substring(0, start) + updateFunc(str.substring(start, end)) + str.substr(end)

        // Tab
        if (keys.length === 1 && keys.includes('Tab')) {
            selectionIndex += 2
            code = changeAt(code, selectionStart, selectionEnd, (editable) => {
                return '  '
            })

        } else if (keys.length === 1 && keys.includes('Enter')) {
            const line = this.convertIndexToLine(code, selectionStart)
            const prevLine = this.prevLineByIndex(code, line)

            let prefix = prevLine ? prevLine.match(/^\s+/s) : ''
            prefix = prefix ? prefix : ''
            console.log('prefix', '|' + prefix + '|', prefix.length)
            //if (prevLine && prevLine.endsWith(':'))
            //    prefix = prefix + '  '

            if (prefix !== '')
                selectionIndex += prefix.split(" ").length - 1
            selectionIndex += + 1

            code = changeAt(code, selectionStart, selectionEnd, (editable) => {
                return '\n' + prefix
            })
        }

        if (code !== this.state.code)
            this.setState({code})

        textarea && setTimeout(async function () {
            textarea.selectionStart = selectionIndex
            textarea.selectionEnd = selectionIndex
        }, 0);

        this.pressedKeys = []*/
        return true
    }

    handleKeyDown(e) {
        if (e.key === 'Tab')
            e.preventDefault()
        if (e.key === 'Enter')
            e.preventDefault()

        this.pressedKeys.push(e.key)
    }

    changeSelectedLines(e) {
        const {textarea} = this.state
        if (!textarea)
            return false

        const {selectionStart, selectionEnd, value} = textarea
        const selectedLines = []

        let line = 0, added = false;
        value.split('').map((char, i) => {
            if (char === '\n') {
                line++;
                added = false
            }
            if (!added && i + 1 >= selectionStart && i + 1 <= selectionEnd) {
                selectedLines.push(line)
                added = true
            }
        })

        this.setState({selectedLines})
        return true
    }

    convertIndexToLine(value, index) {
        let line = 0
        const splited = value.split('\n')

        let elem, i = 0, curIndex = 0
        while (i < splited.length) {
            elem = splited[i]
            curIndex += elem.length + 1
            if (curIndex > index)
                return i
            i += 1
        }
    }

    prevLineByIndex(value, index) {
        let line = index
        const splited = value.split('\n')
        while (line > 0) {
            if (!isEmpty(splited[line]))
                return splited[line]

            line -= 1
        }
    }

    render() {
        const {code} = this.state;
        const lines = code.split('\n')

        return (
            <>
                <header className="header">
                    <Header
                        match={this.props.match}
                        onRun={() => this.runProject()}
                        onSave={() => this.saveCode()}
                        onUpload={() => this.uploadCode()}
                        onStop={() => this.stopProject()}
                    />
                </header>

                <div style={{display: 'block'}}>
                    <div className="ide-main">
                        <div className="ide-counter">
                            {
                                lines.map((line, k) => {
                                    return (
                                        this.state.selectedLines.includes(k) ?
                                            <div className="selected-counter" key={k}>{k + 1}</div>
                                            :
                                            <div key={k}>{k + 1}</div>
                                    )
                                })
                            }
                        </div>
                        <div className="ide">
                            <div className="ide-input">
                            <textarea
                                ref={ref => this.state.textarea = ref}
                                autoFocus={true}
                                value={code}
                                rows={lines.length}
                                onChange={(e) => this.onChange(e)}
                                onMouseUp={(e) => this.changeSelectedLines(e)}
                                spellCheck="false"
                                wrap="off"
                            />
                            </div>

                            <div style={{display: 'flex', flexDirection: 'column'}}>
                                <Highlight language="python">
                                    {code}
                                </Highlight>
                            </div>
                        </div>
                    </div>

                </div>
            </>
        )
    }
}

const mapDispatchToState = (state) => ({
    login: state.login
})

const mapStateToProps = {
    addSnackbarEntry
}

export default connect(mapDispatchToState, mapStateToProps)(Ide)