import React from "react";
import './style.css'
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import {taskTypeToIconWithDescription} from "../tasktype";
import Parser from 'html-react-parser';
import ProgressBar from "../ProgressBar";
import SimpleHeader from "../headers/SimpleHeader";
import Button from "@material-ui/core/Button";
import StarRoundedIcon from '@material-ui/icons/StarRounded';
import {get, post} from "../../service/api";
import {addSnackbarEntry, disposeSnackbarEntry} from "../../actions/snackbars-action";
import {connect} from "react-redux";

class Lesson extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            lesson: undefined,
            profile: undefined,
            tasks: [],
            openSpoiler: false
        }
    }

    async updateTasks(userId, lessonId) {
        get(`/profile/lesson/${userId}/${lessonId}`)
                .then(response => this.setState({profile: response.data}))
                .catch(error => console.error(error.data))

        const parent = this
        setTimeout(async function () {
            parent.updateTasks(userId, lessonId)
        }, 3000);
    }

    async componentDidMount() {
        const userId = localStorage.getItem('id')
        const {lessonId} = this.props.match.params

        this.updateTasks(userId, lessonId)

        const saveLesson = async (lesson) => {
            this.setState({lesson: lesson})
            await lesson.tasks.forEach((taskId, k) => {
                get(`/task/${taskId}`)
                    .then(response => {
                        this.state.tasks[k] = response.data
                    })
            })
        }

        const loadLesson = async () => {
            await get(`/lesson/${lessonId}`)
                .then(response => saveLesson(response.data))
                .catch(error => console.log(error.data))
        }

        const loadProfile = async () => {
            await get(`/profile/lesson/${userId}/${lessonId}`)
                .then(response => this.setState({profile: response.data}))
                .catch(error => console.error(error.data))
        }

        await loadLesson()
        await loadProfile()
    }

    onSendCheck(task) {
        post(`/profile/check/${task.id}`).then(
            this.props.addSnackbarEntry('success', 'Отправлено на проверку!')
        )
    }

    onOpenProject(task) {
        const userId = localStorage.getItem('id')
        get(`/ide/project/${userId}/${task.id}`)
            .then(response => {
                const {url} = response.data
                window.open(`/project/${url}/main.py`, "_blank")
            })
    }

    render() {
        const {tasks, profile, lesson} = this.state

        const findTaskById = (taskId) => tasks.find(task => task && task.id === taskId)
        const findProfileById = (taskId) => profile.tasks.find(task => task.taskId === taskId)

        const detectTaskPreviewIcon = (profile) => {
            if (profile.completed)
                return taskTypeToIconWithDescription('DONE')
            else if (profile.checkable)
                return taskTypeToIconWithDescription('CHECK')
            else if (profile.virgin)
                return taskTypeToIconWithDescription('CLEAR')
            else
                return taskTypeToIconWithDescription('NOCONFIRM')
        }

        return (
            <>
                <SimpleHeader/>
                {
                    lesson &&
                    <div className="lesson-page">
                        <h2>{lesson.title}</h2>
                        <div className="lesson">

                            <div className="description">
                                {Parser(lesson.content)}
                            </div>

                            <ProgressBar percent={70}/>
                        </div>

                        <div className="tasks">
                            <h2>Задачи урока</h2>

                            <div className="task">
                                {
                                    tasks.length !== 0 && profile && profile.tasks.length === lesson.tasks.length && lesson.tasks.length === tasks.length
                                        ? lesson.tasks.map(taskId => {
                                        const task = findTaskById(taskId)
                                        const profile = findProfileById(taskId)

                                        return task && (

                                            <ExpansionPanel key={taskId}>
                                                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
                                                    {detectTaskPreviewIcon(profile)}
                                                    <h3>{task.title}</h3>
                                                    <div style={{marginLeft: 'auto'}}>
                                                        {task.type !== 'ADVANCED' && task.homework && taskTypeToIconWithDescription('HOME')}
                                                        {taskTypeToIconWithDescription(task.type)}
                                                    </div>
                                                </ExpansionPanelSummary>
                                                <ExpansionPanelDetails>
                                                    <div style={{width: '100%'}}>
                                                        {this.renderStars(task.maxStars, profile.stars)}

                                                        <div className="description">
                                                            {Parser(task.description)}
                                                        </div>

                                                        <div className="buttons">

                                                            <Button className="button"
                                                                    variant="contained"
                                                                    disabled={!task.homework}
                                                                    onClick={() => this.onSendCheck(task)}
                                                            >
                                                                Отправить на проверку
                                                            </Button>



                                                            <Button className="button" color="primary"
                                                                    variant="contained"
                                                                    onClick={()=> this.onOpenProject(task)}
                                                            >
                                                                Открыть проект
                                                            </Button>
                                                        </div>
                                                    </div>
                                                </ExpansionPanelDetails>
                                            </ExpansionPanel>
                                        )
                                    })
                                        :
                                        <>
                                            <div style={{
                                                backgroundColor: '#fff',
                                                width: 'auto',
                                                padding: '10px 0',
                                                borderRadius: '4px',
                                                display: 'flex',
                                                flexDirection: 'row',
                                                margin: '3px'
                                            }}>
                                                <div style={{
                                                    backgroundColor: '#cccfd3',
                                                    width: '320px',
                                                    maxWidth: '320px',
                                                    height: '36px',
                                                    margin: '0 0 0 16px',
                                                    display: 'block',
                                                    borderRadius: '4px'
                                                }}/>
                                            </div>
                                            <div style={{
                                                backgroundColor: '#fff',
                                                width: 'auto',
                                                padding: '10px 0',
                                                borderRadius: '4px',
                                                display: 'flex',
                                                flexDirection: 'row',
                                                margin: '3px'
                                            }}>
                                                <div style={{
                                                    backgroundColor: '#cccfd3',
                                                    width: '320px',
                                                    maxWidth: '320px',
                                                    height: '36px',
                                                    margin: '0 0 0 16px',
                                                    display: 'block',
                                                    borderRadius: '4px'
                                                }}/>
                                            </div>
                                            <div style={{
                                                backgroundColor: '#fff',
                                                width: 'auto',
                                                padding: '10px 0',
                                                borderRadius: '4px',
                                                display: 'flex',
                                                flexDirection: 'row',
                                                margin: '3px'
                                            }}>
                                                <div style={{
                                                    backgroundColor: '#cccfd3',
                                                    width: '320px',
                                                    maxWidth: '320px',
                                                    height: '36px',
                                                    margin: '0 0 0 16px',
                                                    display: 'block',
                                                    borderRadius: '4px'
                                                }}/>
                                            </div>
                                        </>
                                }
                            </div>

                        </div>
                    </div>

                }
            </>
        );
    }

    renderStars = (all, have) => {
        const arr = []

        for (let i = 0; i < have; i++) {
            arr.push(<StarRoundedIcon key={i} style={{color: 'orange'}}/>)
        }

        for (let i = 0; i < all - have; i++) {
            arr.push(<StarRoundedIcon key={all - have + i + 1} style={{color: 'silver'}}/>)
        }

        return (
            <div className="stars-bar">
                {arr.map(star => star)}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    tree: state.snackbars.tree,
    message: state.snackbars.message
})

const mapDispatchToProps = {
    addSnackbarEntry,
    disposeSnackbarEntry
}

export default connect(mapStateToProps, mapDispatchToProps)(Lesson)