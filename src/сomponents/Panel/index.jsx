import React from 'react'
import {Route, Switch} from 'react-router-dom'
import LoginForm from "../LoginForm";
import SnackbarTree from "../SnackbarTree";
import {connect} from "react-redux";
import {checkAuth} from "../../actions/login-action";
import ProjectList from "../ProjectList";
import Ide from "../Ide";
import MainPage from "../MainPage";
import ModulesList from "../ModulesList";
import Lesson from "../Lesson";
import UserProfile from "../UserProfile";
import Starter from "../Starter";

class Panel extends React.Component {

    constructor(props) {
        super(props)
        this.routes = [
            {
                path: '/modules/:courseId',
                component: ModulesList
            },
            {
                path: '/project/:url/:file',
                component: Ide
            },
            {
                path: '/courses/',
                component: ModulesList
            },
            {
                path: '/lesson/:lessonId',
                component: Lesson
            },
            {
                path: '/projects',
                component: ProjectList
            },
            {
                path: '/user/:userId',
                component: UserProfile
            },
            {
                path: '/welcome',
                component: Starter
            }
        ]
    }

    componentDidMount() {
        this.props.checkAuth()
    }


    render() {
        return (
            <>
                <SnackbarTree/>

                <Switch>
                    {!this.props.isLoggedIn ?
                        <LoginForm/>
                        :
                        <>
                            <Route exact path='/'>
                                <MainPage/>
                            </Route>

                            {this.routes.map((route, index) => (
                                <Route key={index} path={route.path} component={route.component}/>
                            ))}
                        </>

                    }
                </Switch>
            </>
        );
    }

}

const mapStateToProps = (state) => ({
    isLoggedIn: state.login.isLoggedIn
})

const mapDispatchToProps = {
    checkAuth
}

export default connect(mapStateToProps, mapDispatchToProps)(Panel)