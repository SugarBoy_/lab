import React from "react";
import '../MainPage/style.css'
import './style.css'
import ProgressBar from "../ProgressBar";
import {get} from "../../service/api"
import {Redirect} from "react-router-dom";

class PlayerProfile extends React.Component {

    constructor(props) {
        super(props)

        const userId = props.userId ? props.userId : localStorage.getItem('id')
        const isProfile = props.isProfile ? props.isProfile : false
        this.state = {
            userId: userId,
            isProfile: isProfile,
            profile: undefined,
            stats: {level: 'Новичок'}
        }
    }

    async componentDidMount() {
        const {userId} = this.state
        await get(`/user/${userId}`)
            .then(response => {
                this.setState({profile: response.data})
            })
            .catch(error => {
                console.log(error.data)
            })
    }

    onNotification() {

    }

    render() {
        const {profile, stats, isProfile} = this.state

        return (
            <>
                {profile && !profile.name && <Redirect to="/welcome"/>}

                {profile &&

                <div className="paper">

                    <div className="div-row">

                        <div style={{margin: '16px'}}>
                            <h3>{profile.playername}</h3>
                            <div className="logo"/>
                        </div>

                        <div className="right">
                            {isProfile &&
                            <div className="notification" onClick={() => this.onNotification()}>
                                Уведомлений: <b>0</b>
                            </div>
                            }

                            <div className="field">
                                <b>Имя:</b><br/>
                                {profile.name}
                            </div>

                            <div className="field">
                                <b>Почта:</b><br/>
                                {profile.email}
                            </div>

                            <div className="progress">
                                <div className="div-row">
                                    <div>
                                        <b>Твой ранг:</b> {stats.level}
                                    </div>
                                    <div style={{marginLeft: 'auto'}}>
                                        <b>35%</b>
                                    </div>
                                </div>
                                <ProgressBar percent={35}/>
                            </div>
                        </div>
                    </div>

                </div>
                }
            </>
        );
    }
}

export default PlayerProfile