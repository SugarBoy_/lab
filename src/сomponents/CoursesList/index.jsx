import React from "react";
import '../MainPage/style.css'
import './style.css'
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import Button from "@material-ui/core/Button";
import AssignmentIcon from '@material-ui/icons/Assignment';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import ProgressBar from "../ProgressBar";
import {get} from "../../service/api";
import {Link} from "react-router-dom";

class CoursesList extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            stats: undefined,
            courses: []
        }
    }

    async componentDidMount() {
        const userId = localStorage.getItem('id')

        const loadStats = async (userId) => {
            await get(`/profile/course/${userId}/all`)
                .then(async response => {
                    const stats = response.data
                    this.setState({stats})

                    await stats
                        .filter(course => course.available)
                        .map(async course => await loadCourse(course.courseId))
                })
                .catch(error => {
                    console.log(error.data)
                })
        }

        const loadCourse = async (courseId) => {
            await get(`/course/${courseId}`)
                .then(response => {
                    const course = response.data
                    const {courses} = this.state
                    courses.push(course)
                    this.setState({})
                })
        }

        await loadStats(userId)
    }

    findStatsByCourseId(courseId) {
        const {stats} = this.state
        return stats.find(course => course.courseId === courseId)
    }

    render() {
        const style = {
            icon: {width: '40%', height: 'auto', display: 'block', margin: '0 auto'}
        }

        const {courses} = this.state

        return (
            <div className="course">
                <div>
                    <h3>Доступные курсы</h3>
                </div>

                {courses.map(course => {
                    const stat = this.findStatsByCourseId(course.id)
                    const progress = stat.progress * 100
                    return (
                    <div className="paper" key={course.id}>
                        <h4>{course.title}</h4>
                        <div className="div-row">
                            <div className="field">
                                <Button component={Link} to={`/modules/${course.id}`}>
                                    <div className="div-column">
                                        <div>
                                            <FormatListBulletedIcon style={style.icon}/>
                                        </div>
                                        <div>
                                            <h4>Задачи</h4>
                                        </div>
                                    </div>
                                </Button>
                            </div>

                            <div className="field">
                                <Button disabled>
                                    <div className="div-column">
                                        <div>
                                            <AssignmentIcon style={style.icon}/>
                                        </div>
                                        <div>
                                            <h4>Статьи</h4>
                                        </div>
                                    </div>
                                </Button>
                            </div>

                            <div className="field">
                                <Button disabled>
                                    <div className="div-column">
                                        <div>
                                            <MoreHorizIcon style={style.icon}/>
                                        </div>
                                        <div>
                                            <h4>Прочее</h4>
                                        </div>
                                    </div>
                                </Button>
                            </div>
                        </div>

                        <div className="bar">
                            <div className="div-row">
                                <div>
                                    <b>Пройдено из курса:</b>
                                </div>
                                <div style={{marginLeft: 'auto'}}>
                                    <b>{progress.toFixed(1)}%</b>
                                </div>
                            </div>
                            <ProgressBar percent={progress}/>
                        </div>
                    </div>
                )})
                }
            </div>
        );
    }
}

export default CoursesList