import React from "react";
import './style.css'
import {Redirect} from "react-router-dom";
import {get, patch} from "../../service/api";

class Starter extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            stage: 0,
            name: '',
            playername: '',
            phone: '',
            email: '',
            parentName: '',
            parentEmail: '',
            parentPhone: '',
            isLoad: false
        }
    }

    async componentDidMount() {
        const userId = localStorage.getItem('id')
        await get(`/user/personal/${userId}`)
            .then(response => {
                const {name, playername, email, parentName, parentPhone, parentEmail} = response.data
                this.setState({
                    name,
                    playername,
                    email,
                    parentName,
                    parentPhone,
                    parentEmail
                })
            })
            .catch(error => {
                console.log(error.data)
            })
        this.setState({isLoad: true})
    }

    async saveData() {
        const userId = localStorage.getItem('id')
        const {name, playername, email, parentName, parentPhone, parentEmail} = this.state
        const data = {
            id: userId,
            name: name === null ? '' : name,
            playername: playername === null ? '' : playername,
            parentName: parentName === null ? '' : parentName,
            parentPhone: parentPhone === null ? '' : parentPhone,
            parentEmail: parentEmail === null ? '' : parentEmail
        }
        patch('/user/', data)
    }

    onClick(e) {
        if (e.key === 'Enter') {
            this.setState(
                {
                    stage: this.state.stage + 1,
                }
            )
        }
    }

    onChange(e, field) {
        switch (field) {
            case 'name':
                this.setState({name: e.target.value})
                break
            case 'playername':
                this.setState({playername: e.target.value})
                break
            case 'parentName':
                this.setState({parentName: e.target.value})
                break
            case 'email':
                this.setState({email: e.target.value})
                break
            case 'parentEmail':
                this.setState({parentEmail: e.target.value})
                break
            case 'parentPhone':
                this.setState({parentPhone: e.target.value})
                break
        }
    }


    render() {
        const {stage, isLoad} = this.state

        if (!isLoad)
            return <></>

        var element;
        switch (stage) {
            case 0:
            case 1:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Привет!</span>
                    </div>
                )
                break
            case 2:
            case 3:
                this.nextStage(stage % 2 === 0 ? 3: 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Наверное, ты хочешь приступить скорее к обучению, да?</span>
                    </div>
                )
                break
            case 4:
            case 5:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Хорошо, давай только познакомимся</span>
                    </div>
                )
                break
            case 6:
            case 7:
                if (stage % 2 !== 0)
                    this.nextStage(1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Позови кого-нибудь из родителей, они помогут тебе с этим</span>
                        <button onClick={() => this.nextStage(0.5)}>Они тут</button>
                    </div>
                )
                break
            case 8:
            case 9:
                if (stage % 2 !== 0)
                    this.nextStage(1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Итак, начнем. Как тебя зовут?</span>
                        <input
                            autoFocus={true}
                            value={this.state.name}
                            onKeyUp={e => this.onClick(e)}
                            onChange={e => this.onChange(e, 'name')}
                        />
                    </div>
                )
                break
            case 10:
            case 11:
                if (stage % 2 !== 0)
                    this.nextStage(1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Хорошо, {this.state.name}, а как тебя будут звать в игре?</span><br/>
                        <input
                            value={this.state.playername}
                            autoFocus={true}
                            onKeyUp={e => this.onClick(e)}
                            onChange={e => this.onChange(e, 'playername')}
                        />
                    </div>
                )
                break
            case 12:
            case 13:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Теперь пару вопросов одному из взрослых</span>
                    </div>
                )
                break
            case 14:
            case 15:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Надеюсь, ты не против</span>
                    </div>
                )
                break
            case 16:
            case 17:
                if (stage % 2 !== 0)
                    this.nextStage(1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Уважаемые родитель, можно я узнаю, как Вас зовут?</span>
                        <input
                            value={this.state.parentName}
                            autoFocus={true}
                            onKeyUp={e => this.onClick(e)}
                            onChange={e => this.onChange(e, 'parentName')}
                        />
                    </div>
                )
                break
            case 18:
            case 19:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Приятно познакомиться, {this.state.parentName}</span>
                    </div>
                )
                break
            case 20:
            case 21:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Сейчас надо будет указать почтовый ящик ребенка</span>
                    </div>
                )
                break
            case 22:
            case 23:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Именно на него будут приходить все учебные уведомления</span>
                    </div>
                )
                break
            case 24:
            case 25:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Но если такого нет, то вы можете указать свой</span>
                    </div>
                )
                break
            case 26:
            case 27:
                if (stage % 2 !== 0)
                    this.nextStage(1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Почтовый ящик ребенка:</span>
                        <input
                            value={this.state.email}
                            autoFocus={true}
                            onKeyUp={e => this.onClick(e)}
                            onChange={e => this.onChange(e, 'email')}
                        />
                    </div>
                )
                break
            case 28:
            case 29:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>{this.state.parentName}, а теперь и Ваш личный почтовый ящик</span>
                    </div>
                )
                break
            case 30:
            case 31:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>На него мы будем присылать отчеты об успеваемости</span>
                    </div>
                )
                break
            case 32:
            case 33:
                if (stage % 2 !== 0)
                    this.nextStage(1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Почтовый ящик взрослого:</span>
                        <input
                            value={this.state.parentEmail}
                            autoFocus={true}
                            onKeyUp={e => this.onClick(e)}
                            onChange={e => this.onChange(e, 'parentEmail')}
                        />
                    </div>
                )
                break
            case 34:
            case 35:
                this.nextStage(stage % 2 === 0 ? 2.5 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Могут возникнуть непредвиденные обстоятельства</span>
                    </div>
                )
                break
            case 36:
            case 37:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Укажите номер телефона, чтобы мы могли Вам сообщить о них</span>
                    </div>
                )
                break
            case 38:
            case 39:
                if (stage % 2 !== 0)
                    this.nextStage(1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>{this.state.parentName}, Ваш номер телефона:</span>
                        <input
                            value={this.state.parentPhone}
                            autoFocus={true}
                            onKeyUp={e => this.onClick(e)}
                            onChange={e => this.onChange(e, 'parentPhone')}
                        />
                    </div>
                )
                break
            case 40:
            case 41:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Кажется, это всё</span>
                    </div>
                )
                break
            case 42:
                this.saveData()
            case 43:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Теперь можно приступать к обучению</span>
                    </div>
                )
                break
            case 44:
            case 45:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Создаём профиль игрока {this.state.playername}</span>
                    </div>
                )
                break
            case 46:
            case 47:
                this.nextStage(stage % 2 === 0 ? 3 : 1.5)
                element = (
                    <div className={stage % 2 === 0 ? "start" : "end"}>
                        <span>Успешного изучения!</span>
                    </div>
                )
                break
            case 48:
                return (
                    <Redirect to="/"/>
                )
        }

        return (
            <div className="welcome">
                {element}
            </div>
        )
    }


    async nextStage(seconds) {
        setTimeout(() => {
            this.setState({stage: this.state.stage + 1})
        }, seconds * 1000);
    }
}

export default Starter