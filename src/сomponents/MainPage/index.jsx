import React from "react";
import PlayerProfile from "../PlayerProfile";
import CoursesList from "../CoursesList";
import ProjectList from "../ProjectList";
import SimpleHeader from "../headers/SimpleHeader";

class MainPage extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="main">
                <SimpleHeader hasBack={false}/>
                <PlayerProfile isProfile={true}/>
                <CoursesList/>
                <ProjectList/>
            </div>
        )
    }

}

export default MainPage