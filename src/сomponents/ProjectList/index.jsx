import React from 'react'
import {addSnackbarEntry} from "../../actions/snackbars-action";
import mapDispatchToProps from "react-redux/es/connect/mapDispatchToProps";
import {connect} from "react-redux";
import {get} from '../../service/api'
import Button from "@material-ui/core/Button";
import './style.css'
import '../MainPage/style.css'
import {Link} from "react-router-dom";

class ProjectList extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            list: []
        }
    }

    componentDidMount() {
        this.loadProjects()
    }

    async loadProjects() {
        await get('/ide/project/all')
            .then(response => {
                this.setState({
                    list: response.data
                })
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        const {list} = this.state

        const style = {
            button: {width: '100%', }
        }

        return (
            <div>

                <div className="projects">
                    <h3>Доступные Вам проекты: </h3>

                    <div className="paper">

                        <div className="project-list">
                            {
                                list.map((project) => (
                                    <div className="project" key={project.id}>

                                        <Button
                                            style={style.button}
                                            component={Link}
                                            to={'/project/' + project.url + "/main.py"}
                                        >
                                            <h3>{project.name}</h3>
                                        </Button>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

const mpDispatchToProps = {
    addSnackbarEntry
}

export default connect(null, mapDispatchToProps)(ProjectList)